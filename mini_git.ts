import * as crypto from "crypto"
import * as fs from "fs"
import * as zlib from "zlib"
import path from "path"

const DEBUG = true
const ENTRIES_TO_IGNORE = [".git"]
const SOURCE_FOLDER = "/Users/cassiano/tmp/Snap-5.4.5"
const TARGET_CHECKOUT_FOLDER = "/Users/cassiano/tmp/Snap-5.4.5_checkout"

enum FileMode {
  Tree = "40000",
  StandardFileBlob = "100644",
  ExecutableBlob = "100755",
  SymbolicLinkBlob = "120000",
}

type HexNumber = string
type Sha1 = string
type GitObject = "commit" | "tree" | "blob"
type Encoding = "utf8" | "binary"

interface AbstractTreeEntry {
  mode: FileMode
  name: string
  sha1: Sha1
}

interface BlobTreeEntry extends AbstractTreeEntry {
  mode:
    | FileMode.StandardFileBlob
    | FileMode.ExecutableBlob
    | FileMode.SymbolicLinkBlob
  blob: Blob
}

interface TreeTreeEntry extends AbstractTreeEntry {
  mode: FileMode.Tree
  tree: Tree
}

type TreeEntry = BlobTreeEntry | TreeTreeEntry

interface Tree {
  size: number
  entries: TreeEntry[]
}

interface Developer {
  name: string
  timestamp: number
  timezone: string
}

interface Commit {
  size: number
  treeSha1: Sha1
  parents: Sha1[]
  author: Developer
  committer: Developer
  message: string
}

interface Blob {
  size: number
  contents: string
}

class GitRepository {
  basePath: string

  constructor(basePath: string) {
    this.basePath = basePath
  }

  createBlob(contents: Buffer) {
    return this._findOrCreateObject("blob", contents)
  }

  createTree(contents: Buffer) {
    return this._findOrCreateObject("tree", contents)
  }

  createTreeFromFolder(folderPath: string, level: number = 0) {
    const entries = fs
      .readdirSync(folderPath)
      .filter((entry: string) => ENTRIES_TO_IGNORE.indexOf(entry) === -1)
      .sort()

    const treeEntries = entries.map((entry: string, i: number) => {
      const filePath = path.join(folderPath, entry)
      const entryStat = fs.statSync(filePath)
      const entryLstat = fs.lstatSync(filePath)
      const isFolder = entryStat.isDirectory()
      const userGroupAndOthersExecutableBitsSet = 0b001_001_001
      const isSymbolicLink = entryLstat.isSymbolicLink()
      const isExecutable =
        !isSymbolicLink &&
        (entryStat.mode & userGroupAndOthersExecutableBitsSet) ===
          userGroupAndOthersExecutableBitsSet

      if (DEBUG) {
        console.log(
          [
            " ".repeat(level * 3),
            isFolder ? "└" : i < entries.length - 1 ? "├" : "└",
            "─ ",
            entry,
            isFolder ? ":" : "",
          ].join("")
        )
      }

      let mode: FileMode
      let sha1: Sha1

      if (isFolder) {
        mode = FileMode.Tree
        sha1 = this.createTreeFromFolder(filePath, level + 1)
      } else {
        let blobContents: Buffer

        if (isExecutable) {
          mode = FileMode.ExecutableBlob
          blobContents = fs.readFileSync(filePath)
        } else if (isSymbolicLink) {
          mode = FileMode.SymbolicLinkBlob
          blobContents = Buffer.from(fs.readlinkSync(filePath))
        } else {
          mode = FileMode.StandardFileBlob
          blobContents = fs.readFileSync(filePath)
        }

        sha1 = this.createBlob(blobContents)
      }

      return Buffer.concat([
        Buffer.from(`${mode} ${entry}\x00`),
        Buffer.from(this._hexToBytes(sha1)),
      ])
    })

    const treeSha1 = this.createTree(
      treeEntries.reduce((acc: Buffer, item: Buffer) =>
        Buffer.concat([acc, item])
      )
    )

    return treeSha1
  }

  createCommit(
    treeSha1: Sha1,
    parentsSha1s: Sha1[],
    author: string,
    committer: string,
    message: string
  ) {
    const parentsText = parentsSha1s
      .map(parentSha1 => `parent ${parentSha1}`)
      .join("\n")

    const currentTime = Math.floor(new Date().getTime() / 1000)

    return this._findOrCreateObject(
      "commit",
      Buffer.from(
        this._cleanse(`
          ${parentsText}
          tree ${treeSha1}
          author ${author} ${currentTime} -0300
          committer ${committer} ${currentTime} -0300

          ${message}
        `) + "\n"
      )
    )
  }

  createBranch(name: string, sha1: Sha1) {
    const branchesPath = this.branchesPath()

    this._createPathIfNonExistent(branchesPath)

    fs.writeFileSync(path.join(branchesPath, name), sha1)
  }

  checkout(branchName: string, targetCheckoutFolder: string) {
    const branchPath = path.join(this.branchesPath(), branchName)
    const branchSha1 = fs.readFileSync(branchPath).toString() as Sha1
    const commit = this._readCommit(branchSha1)
    const tree = this._readTree(commit.treeSha1)

    this._checkoutTree(tree, targetCheckoutFolder)
  }

  _setDiff(oneSet: Set<any>, anotherSet: Set<any>) {
    return new Set([...oneSet].filter(item => !anotherSet.has(item)))
  }

  _checkoutTree(tree: Tree, baseFolder: string) {
    if (this._pathExists(baseFolder)) {
      const entries = fs.readdirSync(baseFolder)
      const entriesToDelete = this._setDiff(
        new Set(entries),
        new Set(tree.entries.map(entry => entry.name))
      )

      // Delete all existing entries (files, folders, symlinks etc) not belonging to the current folder.
      entriesToDelete.forEach(entry => fs.rmSync(path.join(baseFolder, entry)))
    } else {
      this._createPath(baseFolder)
    }

    tree.entries.forEach(entry => {
      const entryPath = path.join(baseFolder, entry.name)

      switch (entry.mode) {
        case FileMode.StandardFileBlob:
        case FileMode.ExecutableBlob:
          fs.writeFileSync(entryPath, entry.blob.contents, {
            encoding: "binary",
            mode: entry.mode === FileMode.StandardFileBlob ? 0o644 : 0o755,
          })
          break

        case FileMode.SymbolicLinkBlob:
          if (this._pathExists(entryPath)) fs.rmSync(entryPath)

          fs.symlinkSync(entry.blob.contents, entryPath, "file")
          break

        case FileMode.Tree:
          this._checkoutTree(entry.tree, entryPath)
          break

        default:
          const _exhaustiveCheck: never = entry
          throw `Unexpected tree entry mode ${entry.mode} for entry with SHA1 ${entry.sha1}`
      }
    })
  }

  branchesPath() {
    return path.join(this._gitPath(), "refs", "heads")
  }

  _readCommit(sha1: Sha1): Commit {
    const { size, contents } = this._parseObject(sha1, "commit", "utf8")
    const matchResults = contents.match(
      this._regExpJoin(
        /^tree /,
        /(?<treeSha1>[0-9a-fA-F]{40})\n/,
        /(?<parents>(parent [0-9a-fA-F]{40}\n)*)/,
        /author (?<author>.+) (?<authorTimestamp>\d+) (?<authorTimezone>[-+]?\d{4})\n/,
        /committer (?<committer>.+) (?<committerTimestamp>\d+) (?<committerTimezone>[-+]?\d{4})\n/,
        /\n(?<message>[\x00-\xFF]+)$/
      )
    )

    if (!matchResults || !matchResults.groups)
      throw `Error parsing commit object with SHA1 ${sha1} in _readCommit().`

    const parentsMatch = matchResults.groups.parents
    const parents =
      parentsMatch === null
        ? []
        : (
            this._extractRepeatingPattern(
              parentsMatch,
              /parent (?<sha1>[0-9a-fA-F]{40})\n/
            ) as { sha1: Sha1 }[]
          ).map(parent => parent.sha1)

    return {
      size,
      parents,
      treeSha1: matchResults.groups.treeSha1,
      author: {
        name: matchResults.groups.author,
        timestamp: Number(matchResults.groups.authorTimestamp),
        timezone: matchResults.groups.authorTimezone,
      },
      committer: {
        name: matchResults.groups.committer,
        timestamp: Number(matchResults.groups.committerTimestamp),
        timezone: matchResults.groups.committerTimezone,
      },
      message: matchResults.groups.message,
    }
  }

  _regExpJoin(...regExps: RegExp[]): RegExp {
    return new RegExp(regExps.map(re => re.source).join(""))
  }

  _readBlob(sha1: Sha1): Blob {
    return this._parseObject(sha1, "blob", "binary")
  }

  _readTree(sha1: Sha1): Tree {
    const { size, contents } = this._parseObject(sha1, "tree", "binary")

    const extractedEntries = (
      this._extractRepeatingPattern(
        contents,
        /(?<mode>\w+) (?<name>[^\0]+)\0(?<sha1>[\x00-\xFF]{20})/
      ) as Pick<TreeEntry, "mode" | "name" | "sha1">[]
    ).map(entry => {
      const name = this._forceEncoding(entry.name, "binary", "utf8")
      const sha1 = this._bytesToHex([...Buffer.from(entry.sha1, "binary")])

      return { ...entry, name, sha1 }
    })

    const entries: TreeEntry[] = extractedEntries.map(entry => {
      switch (entry.mode) {
        case FileMode.StandardFileBlob:
        case FileMode.ExecutableBlob:
        case FileMode.SymbolicLinkBlob:
          return {
            ...entry,
            blob: this._readBlob(entry.sha1),
          } as BlobTreeEntry

        case FileMode.Tree:
          return {
            ...entry,
            tree: this._readTree(entry.sha1),
          } as TreeTreeEntry

        default:
          const _exhaustiveCheck: never = entry.mode
          throw `Unexpected tree entry mode ${entry.mode} for entry with SHA1 ${entry.sha1}`
      }
    })

    return { size, entries }
  }

  _parseObject(sha1: Sha1, objectType: GitObject, encoding: Encoding): Blob {
    const object = this._readObject(sha1).toString(encoding)
    const matchResults = object.match(
      new RegExp(
        `^${objectType} (?<size>\\d+)\\x00(?<contents>[\\x00-\\xFF]*)$`
      )
    )

    if (!matchResults || !matchResults.groups)
      throw `Error parsing ${objectType} object with SHA1 ${sha1} in _parseObject().`

    const size = Number(matchResults.groups.size)
    const contents = matchResults.groups.contents

    if (contents.length !== size)
      throw `Expected ${objectType} contents length to be ${contents.length}, but was ${size}`

    return { size, contents }
  }

  _forceEncoding(
    text: string,
    sourceEncoding: Encoding,
    targetEncoding: Encoding
  ) {
    return Buffer.from(text, sourceEncoding).toString(targetEncoding)
  }

  _extractRepeatingPattern(source: string, pattern: RegExp) {
    return (
      source
        .match(new RegExp(pattern, "g"))
        ?.map((item: string) => item.match(pattern)?.groups ?? {}) ?? []
    )
  }

  _readObject(sha1: Sha1) {
    const objectPath = path.join(
      this._objectsPath(),
      sha1.slice(0, 2),
      sha1.slice(2)
    )

    return zlib.inflateSync(fs.readFileSync(objectPath))
  }

  _objectsPath() {
    return path.join(this._gitPath(), "objects")
  }

  _findOrCreateObject(type: GitObject, contents: Buffer) {
    const contentsSize = this._byteSize(contents)
    const header = `${type} ${contentsSize}\x00`
    const store = Buffer.concat([Buffer.from(header), Buffer.from(contents)])
    const sha1 = this._sha1(store)
    const objectFolder = path.join(this._objectsPath(), sha1.slice(0, 2))
    const objectPath = path.join(objectFolder, sha1.slice(2))

    if (!this._pathExists(objectPath)) {
      // console.log(`Creating ${type} object with SHA1 ${sha1}`)
      // console.log([store, store.toString()])

      this._createPathIfNonExistent(objectFolder)

      fs.writeFileSync(objectPath, zlib.deflateSync(store))
    }

    return sha1
  }

  _sha1(data: Buffer): Sha1 {
    return crypto
      .createHash("sha1")
      .update(data.toString("binary"), "binary")
      .digest("hex")
  }

  _byteSize(contents: Buffer) {
    return Buffer.from(contents).length
  }

  _gitPath() {
    return path.join(this.basePath, ".git")
  }

  _createPath(path: string) {
    fs.mkdirSync(path, { recursive: true })
  }

  _createPathIfNonExistent(path: string) {
    if (!this._pathExists(path)) this._createPath(path)
  }

  _pathExists(path: string) {
    try {
      fs.accessSync(path)

      return true
    } catch (ENOENT) {
      return false
    }
  }

  _hexToBytes(hex: HexNumber): number[] {
    const parseHex = (hex: string) => parseInt(hex, 16)

    return hex.match(/[0-9a-fA-F]{2}/g)?.map(parseHex) ?? []
  }

  _bytesToHex(bytes: number[]) {
    return bytes.map(byte => byte.toString(16).padStart(2, "0")).join("")
  }

  _cleanse(contents: string) {
    return contents.trim().replaceAll(/^[ ]*/gm, "").replaceAll(/[ ]*$/gm, "")
  }
}

const main = () => {
  const parseArgs = require("minimist")(process.argv.slice(2))

  const gitRepo = new GitRepository(SOURCE_FOLDER)

  // const blobSha1 = gitRepo.createBlob('what is up, doc?')
  // const treeSha1 = this.createTree(
  //   Buffer.concat([
  //     Buffer.from(`${FILE_MODES.blob} sample_file.txt\x00`),
  //     Buffer.from(this._hexToBytes(blobSha1)),
  //   ])
  // )

  if (parseArgs["delete-objects-folder"] === "true") {
    const STANDARD_GIT_OBJECTS_FOLDERS = ["info", "pack"]
    const SOURCE_FOLDER_OBJECTS_FOLDER = path.join(
      SOURCE_FOLDER,
      ".git",
      "objects"
    )

    // Delete all existing Git objects in source folder.
    fs.readdirSync(SOURCE_FOLDER_OBJECTS_FOLDER).forEach((folder: string) => {
      if (STANDARD_GIT_OBJECTS_FOLDERS.indexOf(folder) === -1)
        fs.rmdirSync(path.join(SOURCE_FOLDER_OBJECTS_FOLDER, folder), {
          recursive: true,
        })
    })
  }

  const treeSha1 = gitRepo.createTreeFromFolder(SOURCE_FOLDER)

  const commitSha1 = gitRepo.createCommit(
    treeSha1,
    [],
    "Cassiano D'Andrea <cassiano.dandrea@tagview.com.br>",
    "Cassiano D'Andrea <cassiano.dandrea@tagview.com.br>",
    "First commit"
  )

  gitRepo.createBranch("master", commitSha1)
  console.log(`Updated Git repo in folder '${SOURCE_FOLDER}'.`)

  gitRepo.checkout("master", TARGET_CHECKOUT_FOLDER)

  console.log(
    `Checked out master branch to folder '${TARGET_CHECKOUT_FOLDER}'.`
  )
}

main()

// node --trace-uncaught mini_git.js
// ts-node-transpile-only mini_git.ts --delete-objects-folder=true
// diff ~/tmp/Snap-5.4.5 ~/tmp/Snap-5.4.5_checkout -r
// cd ~/projects/rails/todomvc_react && git fsck
